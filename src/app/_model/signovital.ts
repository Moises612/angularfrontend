import { Paciente } from "./paciente";

export class Signovital {
  id :number;
  paciente:Paciente;
  fecha:string;
  pulso:string;
  ritmorespiratorio:string;
}
