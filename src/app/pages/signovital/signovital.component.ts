import { Router } from '@angular/router';
import { SignovitalService } from './../../_service/signovital.service';
import { Signovital } from './../../_model/signovital';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-signovital',
  templateUrl: './signovital.component.html',
  styleUrls: ['./signovital.component.css']
})
export class SignovitalComponent implements OnInit {

  displayedColumns = ['id', 'nombres', 'apellidos', 'fecha','temperatura','pulso','ritmo','acciones'];
  dataSource: MatTableDataSource<Signovital>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  cantidad: number = 0;

  constructor(
    private signovitalService:SignovitalService,
    private snackBar: MatSnackBar,
    public router: Router
  ) { }

  ngOnInit(): void {

    this.signovitalService.getSignoVitalCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.signovitalService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.signovitalService.listarPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }


  mostrarMas(e: any){
    this.signovitalService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }

  crearTabla(data: Signovital[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminar(id: number) {
    this.signovitalService.eliminar(id).pipe(switchMap(() => {
      return this.signovitalService.listar();
    })).subscribe(data => {
      this.signovitalService.setSignoVitalCambio(data);
      this.signovitalService.setMensajeCambio('SE ELIMINO');
    });
  }



}
