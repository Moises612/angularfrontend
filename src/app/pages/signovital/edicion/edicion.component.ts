import { SignovitalService } from './../../../_service/signovital.service';
import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { debounceTime, flatMap, map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edicion',
  templateUrl: './edicion.component.html',
  styleUrls: ['./edicion.component.css']
})
export class EdicionComponent implements OnInit {

  forma: FormGroup;
  select_paciente:Paciente;
  pacientesFiltrados: Observable<Paciente[]>;
  myControl = new FormControl();
  id:number;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private pacienteService:PacienteService,
    private signovitalService:SignovitalService,
    private activatedRoute: ActivatedRoute
  ) {

    this.activatedRoute.params.subscribe((params) => {
      this.id = params["id"];
    });
  }

  ngOnInit(): void {

    this.pacientesFiltrados = this.myControl.valueChanges.pipe(
      debounceTime(500),
      map((value) => (typeof value === "string" ? value : value.cdescar)),
      flatMap((value) => (value ? this._filter(value) : []))
    );

    this.crearForumlario();
  }

  ngAfterViewInit() {
    this.cargarDataAlFormulario();
  }

  mostrarNombre(paciente?: Paciente): string | undefined {
    return paciente ? paciente.nombres+" "+paciente.apellidos  : undefined;
  }

  private _filter(value: string): Observable<Paciente[]> {
    const filterValue = value.toLowerCase();
    return this.pacienteService.buscarPaciente(filterValue,filterValue);
  }

  seleccionarObjecto(event: MatAutocompleteSelectedEvent){
    this.select_paciente=event.option.value as Paciente;

  }

  crearForumlario(){



    this.forma = this.fb.group({
      id:"",
      paciente:this.myControl,
      fecha:[new Date().toISOString(),Validators.required],
      temperatura:["",Validators.required],
      pulso:["",Validators.required],
      ritmorespiratorio:["",Validators.required]
    });

  }
  guardar(){

    if(this.forma.valid){

      if (this.id){
        this.signovitalService.modificar(this.forma.getRawValue()).subscribe(()=>{
          this.signovitalService.listar().subscribe(data=>{
             this.signovitalService.setSignoVitalCambio(data);
             this.signovitalService.setMensajeCambio('SE REGISTRO');
             this.router.navigate(["/pages/signosvitales"]);
          })

          })

      }
      else{
        this.signovitalService.registrar(this.forma.getRawValue()).subscribe(()=>{
          this.signovitalService.listar().subscribe(data=>{
             this.signovitalService.setSignoVitalCambio(data);
             this.signovitalService.setMensajeCambio('SE REGISTRO');
             this.router.navigate(["/pages/signosvitales"]);
          })

          })

      }


    }

  }

  cargarDataAlFormulario() {
    if (this.id){
      this.signovitalService.listarPorId(this.id).subscribe(data => {
        let data_edit=data;
        data_edit.fecha=new Date(data.fecha).toISOString();
        this.forma.reset(data_edit);
      });

    }

  }

}
