import { LoginService } from 'src/app/_service/login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

 usuario:String;
 perfiles:Array<String>;
 //perfiles:Array;
  constructor(
    private loginService:LoginService
  ) { }

  ngOnInit(): void {
    this.usuario=this.loginService.getCurrentUser();
    this.perfiles=this.loginService.getCurrentUserAuthorities();

  }

}
