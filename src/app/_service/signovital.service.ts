import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Signovital } from '../_model/signovital';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignovitalService extends GenericService<Signovital> {

  private signoVitalCambio = new Subject<Signovital[]>();
  private mensajeCambio = new Subject<string>();
  constructor(protected http: HttpClient) {
    super(http
      ,`${environment.HOST}/signosvitales`);
  }
  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  /* get, set */
  setMensajeCambio(mensaje: string){
    this.mensajeCambio.next(mensaje);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }

  setSignoVitalCambio(lista: Signovital[]){
    this.signoVitalCambio.next(lista);
  }

  getSignoVitalCambio(){
    return this.signoVitalCambio.asObservable();
  }
}
